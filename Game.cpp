#include <stdint.h>
#include "Game.h"

void Game::reset() {
  teamA = teamB = 0;
}

Game::Game(void) {
  reset();
}

bool Game::isFinished() {
  return teamA > 5 || teamB > 5;
}

int8_t Game::getPointsA() {
  return teamA;
}

int8_t Game::getPointsB() {
  return teamB;
}

void Game::goalA() {
  if(!isFinished()) {
    teamA++;
  }
}

void Game::goalB() {
  if(!isFinished()) {
    teamB++;
  }
}
