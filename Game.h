#ifndef _GAME_H__
#define _GAME_H__
#include <stdint.h>

class Game
{
  public:
    Game();
    int8_t getPointsA();
    int8_t getPointsB();
    bool isFinished();
    void reset();
    void goalA();
    void goalB();

  private:
    int8_t teamA;
    int8_t teamB;
};

#endif
