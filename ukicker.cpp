#include <avr/io.h>
#include <avr/interrupt.h>

#include "Game.h"
#include "WS2812.h"

// LEDs  -- PB0 (OC0A)
// GoalA -- PB1 (PCINT1)
// GoalB -- PB2 (PCINT2)
// Reset -- PB3 (PCINT3)
// NC    -- PB4
// NC    -- PB5

#define TIMEOUT_MAX 65535
#define MAP_GOAL_A 0b00000010
#define MAP_GOAL_B 0b00000100
#define MAP_RESET  0b00001000

Game     GAME;
WS2812   LED(12);
cRGB     RED   = { 255,   0,   0 };
cRGB     GREEN = {   0, 255,   0 };
cRGB     BLACK = {   0,   0,   0 };
// This will be set to TIMEOUT_MAX when a goal is detected. With the
// timer0 overflow it will be decreased until it reaches 0. Goals will
// only be counted, if the TIMEOUT is set to 0. To prevent any hickups
// when starting, the TIMEOUT is set to TIMEOUT_MAX at startup to give
// it time before registering the first goals.
uint16_t TIMEOUT = 0;
// This value stores the port-status to compare it with the next run.
uint16_t before = 0;

void setPixels() {
  for(int8_t i = 0; i < 6; i++) {
    LED.set_crgb_at(    i, GAME.getPointsA() <= i + 1 ? ( i > 3 ? RED : GREEN ) : BLACK);
  }
  for(int8_t i = 0; i < 6; i++) {
    LED.set_crgb_at(i + 6, GAME.getPointsB() <= i + 1 ? ( i > 3 ? RED : GREEN ) : BLACK);
  }
  LED.sync();
}

int main()
{
  // LED
  LED.setOutput(&PORTB, &DDRB, PINB5);
  // Setup fast PWM
  TCCR0A |= (1 << COM0A1) | (1 << WGM01) | (1 << WGM00);
  TCCR0B |= (1 << CS02); // 9,6MHz / 256 = 37,5kHz ~ 38kHz
  // Enable interrupts
  TIMSK0 |= (1 << TOIE0);
  sei();

  while(true) {
    uint8_t now = PORTB & 0b00001110;
    uint8_t diff = now ^ before;
    if(diff != 0) {
      if(TIMEOUT == 0) {
        if(!GAME.isFinished()) {
          if(now & MAP_GOAL_A && diff & MAP_GOAL_A) { // GOAL A
            GAME.goalA();
          }
          if(now & MAP_GOAL_B && diff & MAP_GOAL_B) { // GOAL B
            GAME.goalB();
          }
        }
        if(diff & MAP_RESET && now & MAP_RESET) { // RESET
          GAME.reset();
        }
        before = now;
        TIMEOUT--; // set to maximum (via overflow as it's 0 at the moment)
        setPixels();
      }
    }
  }
}

ISR (TIM0_OVF_vect) {
  if(TIMEOUT != 0) {
    TIMEOUT--;
  }
}

